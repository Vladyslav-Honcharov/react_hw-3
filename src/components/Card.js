import React, { useState } from "react";
import "./Card.scss";
import * as bootstrap from "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import Button from "./Button";
import Modal from "./Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faStar as farStar } from "@fortawesome/free-regular-svg-icons";

const Card = ({
  name,
  price,
  url,
  color,
  addToFavorites,
  handleRemoveFromFavorites,
  addToCart,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isFavorite, setIsFavorite] = useState(
    JSON.parse(localStorage.getItem(`isFavorite_${name}_${price}`)) || false
  );
  const storageKey = `isFavorite_${name}_${price}`;

  const toggleFavorite = (e) => {
    e.preventDefault();
    const newFavorite = !isFavorite;
    setIsFavorite(newFavorite);
    if (newFavorite) {
      addToFavorites({ name, price, url });
    } else {
      handleRemoveFromFavorites({ name, price, url });
    }
    localStorage.setItem(storageKey, JSON.stringify(newFavorite));
  };

  const handleClick = () => {
    setIsModalOpen(true);
  };

  const handleClose = () => {
    setIsModalOpen(false);
  };

  const handleModalClick = (e) => {
    if (e.target.classList.contains("modal")) {
      setIsModalOpen(false);
    }
    return;
  };

  const addToCartHandler = () => {
    addToCart({ name, price, url });
    setIsModalOpen(false);
    handleClose();
  };

  return (
    <div className="card" style={{ width: "18rem" }}>
      <div className="favorite-icon">
        <a href="" onClick={toggleFavorite}>
          <FontAwesomeIcon
            icon={isFavorite ? faStar : farStar}
            className={isFavorite ? "star-icon selected" : "star-icon"}
          />
        </a>
      </div>
      <img src={url} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <p className="card-text">{color}</p>
      </div>
      <ul className="list-group list-group-flush">
        <li className="list-group-item">{price} $</li>
      </ul>
      <Button text={"Add to card"} onClick={handleClick} />
      {isModalOpen && (
        <Modal
          header="Do you want to add this wine to your cart?"
          closeButton={true}
          action="AddCard"
          onClose={handleClose}
          btnOne="Ok"
          btnSecond="Cancel"
          onClickOutside={handleModalClick}
          onClick={addToCartHandler}
          name={name}
          price={price}
          url={url}
        />
      )}
    </div>
  );
};

export default Card;
