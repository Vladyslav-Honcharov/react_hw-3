import React, { createContext, useEffect, useState } from "react";
import axios from "axios";

// Создаем контекст
export const DataListContext = createContext();

const DataListProvider = ({ children }) => {
  const [dataList, setDataList] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      const response = await axios.get("data.json");
      setDataList(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // Предоставляем состояние и методы через контекст
  return (
    <DataListContext.Provider value={{ dataList }}>
      {children}
    </DataListContext.Provider>
  );
};

export default DataListProvider;
