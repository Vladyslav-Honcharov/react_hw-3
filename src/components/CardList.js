import "./CardList.scss";
import Card from "./Card";
import React, { useContext } from "react";
import { DataListContext } from "./DataListContext";
import PropTypes from "prop-types";

const CardList = ({
  favoritesList,
  addToCart,
  addToFavorites,
  handleRemoveFromCart,
  handleRemoveFromFavorites,
  saveDataToLocalStorage,
}) => {
  const { dataList } = useContext(DataListContext);

  return (
    <div className="card-list container">
      {dataList.map((item) => (
        <div key={item.arctical}>
          <Card
            url={item.url}
            name={item.name}
            price={item.price}
            color={item.color}
            isFavorite={favoritesList}
            addToCart={addToCart}
            addToFavorites={addToFavorites}
            handleRemoveFromCart={handleRemoveFromCart}
            handleRemoveFromFavorites={handleRemoveFromFavorites}
            saveDataToLocalStorage={saveDataToLocalStorage}
          />
        </div>
      ))}
    </div>
  );
};

CardList.propTypes = {
  favoritesList: PropTypes.array.isRequired,
  addToCart: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  handleRemoveFromCart: PropTypes.func.isRequired,
  handleRemoveFromFavorites: PropTypes.func.isRequired,
  saveDataToLocalStorage: PropTypes.func.isRequired,
};

export default CardList;
