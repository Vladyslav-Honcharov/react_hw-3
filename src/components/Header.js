import React from "react";
import { Link } from "react-router-dom";
import Cart from "./Cart";
import Favorites from "./Favorites";
import "./Header.scss";

const Header = ({
  cartList,
  favoritesList,
  handleRemoveFromCart,
  handleRemoveFromFavorites,
}) => {
  return (
    <div>
      <nav className="navbar">
        <div className="container-fluid">
          <Link to="/" className="navbar-brand">
            CoolWine
          </Link>

          <div className="header-icons">
            <Cart
              cartList={cartList}
              handleRemoveFromCart={handleRemoveFromCart}
            />
            <Favorites
              favoritesList={favoritesList}
              handleRemoveFromFavorites={handleRemoveFromFavorites}
            />
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Header;
