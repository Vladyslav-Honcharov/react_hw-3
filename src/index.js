import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CartPage from "./pages/CartPage";
import FavouritesPage from "./pages/FavouritesPage";
import Errorpage from "./pages/Errorpage";
import { useState, useEffect } from "react";

const el = document.getElementById("root");
const root = ReactDOM.createRoot(el);

export default function Index() {
  const [cartList, setCartList] = useState([]);
  const [favoritesList, setFavoritesList] = useState([]);

  const saveDataToLocalStorage = (newCartList, newFavoritesList) => {
    localStorage.setItem("cartList", JSON.stringify(newCartList));
    localStorage.setItem("favoritesList", JSON.stringify(newFavoritesList));
  };

  const loadDataFromLocalStorage = () => {
    const cartListData = localStorage.getItem("cartList");
    const favoritesListData = localStorage.getItem("favoritesList");

    console.log("cartListData:", cartListData);
    console.log("favoritesListData:", favoritesListData);

    if (cartListData !== null) {
      try {
        setCartList(JSON.parse(cartListData));
      } catch (error) {
        console.error("Error parsing cartListData:", error);
      }
    }

    if (favoritesListData !== null) {
      try {
        setFavoritesList(JSON.parse(favoritesListData));
      } catch (error) {
        console.error("Error parsing favoritesListData:", error);
      }
    }
  };

  useEffect(() => {
    loadDataFromLocalStorage();
  }, []);

  const addToCart = (card) => {
    setCartList((prevCartList) => {
      const newCartList = [...prevCartList, card];
      saveDataToLocalStorage(newCartList, favoritesList);
      return newCartList;
    });
  };

  const addToFavorites = (favorites) => {
    setFavoritesList((prevFavoritesList) => {
      const newFavoritesList = [...prevFavoritesList, favorites];
      saveDataToLocalStorage(cartList, newFavoritesList);
      return newFavoritesList;
    });
  };

  const handleRemoveFromCart = (index) => {
    setCartList((prevCartList) => {
      const newCartList = [...prevCartList];
      newCartList.splice(index, 1);
      return newCartList;
    });
  };

  const handleRemoveFromFavorites = (index) => {
    setFavoritesList((prevFavoritesList) => {
      const newFavoritesList = [...prevFavoritesList];
      newFavoritesList.splice(index, 1);
      return newFavoritesList;
    });
  };

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <App
                cartList={cartList}
                favoritesList={favoritesList}
                handleRemoveFromCart={handleRemoveFromCart}
                handleRemoveFromFavorites={handleRemoveFromFavorites}
                addToCart={addToCart}
                addToFavorites={addToFavorites}
                saveDataToLocalStorage={saveDataToLocalStorage}
              />
            }
          />
          <Route
            path="CartPage"
            element={
              <CartPage
                cartList={cartList}
                favoritesList={favoritesList}
                handleRemoveFromCart={handleRemoveFromCart}
                saveDataToLocalStorage={saveDataToLocalStorage}
              />
            }
          />
          <Route
            path="FavouritesPage"
            element={
              <FavouritesPage
                favoritesList={favoritesList}
                cartList={cartList}
                handleRemoveFromFavorites={handleRemoveFromFavorites}
                saveDataToLocalStorage={saveDataToLocalStorage}
              />
            }
          />
          <Route path="*" element={<Errorpage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

root.render(<Index />);
