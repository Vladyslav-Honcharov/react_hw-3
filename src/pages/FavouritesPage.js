import React from "react";
import Header from "../components/Header";

function FavouritesPage({
  cartList,
  favoritesList,
  handleRemoveFromFavorites,
  saveDataToLocalStorage,
}) {
  const removeFromFavorites = (index, storageKey) => {
    const newFavoritesList = [...favoritesList];
    newFavoritesList.splice(index, 1);
    handleRemoveFromFavorites(index);
    localStorage.removeItem(storageKey);
    saveDataToLocalStorage(cartList, newFavoritesList);
  };

  const renderFavouritesItems = () => {
    return favoritesList.map((item, index) => {
      const storageKey = `isFavorite_${item.name}_${item.price}`;
      return (
        <div className="favourites m-4" key={index}>
          <div className="favourites-text">
            <div>{item.name}</div>
            <div>{item.price} $</div>
          </div>
          <img className="favourites-img" src={item.url} alt="" />
          <div
            className="favourites-close"
            onClick={() => {
              removeFromFavorites(index, storageKey);
            }}
          >
            ♥
          </div>
        </div>
      );
    });
  };

  return (
    <>
      <Header cartList={cartList} favoritesList={favoritesList} />
      <div class="favourites-header">Your favorites wine</div>
      <div class="favourites-body">{renderFavouritesItems()}</div>
    </>
  );
}

export default FavouritesPage;
