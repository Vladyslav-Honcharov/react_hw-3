import React, { useState } from "react";
import Header from "../components/Header";
import Modal from "../components/Modal";

function CartPage({
  cartList,
  favoritesList,
  handleRemoveFromCart,
  saveDataToLocalStorage,
}) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalIndex, setModalIndex] = useState(null);
  const removeFromCart = (index) => {
    setIsModalOpen(true);
    setModalIndex(index);
  };

  const confirmRemoveFromCart = (index) => {
    const newCartList = [...cartList];
    newCartList.splice(index, 1);
    // handleRemoveFromCart(newCartList);

    handleRemoveFromCart(index);

    saveDataToLocalStorage(newCartList, favoritesList);
    setIsModalOpen(false);
  };

  const deleteCard = () => {
    confirmRemoveFromCart(modalIndex);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const renderCartItems = () => {
    return cartList.map((item, index) => (
      <div className="cart m-4" key={index}>
        <div className="cart-text">
          <div>{item.name}</div>
          <div className="cart-price">{item.price} $</div>
          <button className="btn btn-success">Buy</button>
        </div>
        <img className="cart-img" src={item.url} alt="" />
        <div className="cart-close" onClick={() => removeFromCart(index)}>
          X
        </div>
      </div>
    ));
  };

  return (
    <>
      <Header cartList={cartList} favoritesList={favoritesList} />{" "}
      <div className="cart-header">Cart</div>{" "}
      <div className="cart-body">{renderCartItems()}</div>
      {isModalOpen && (
        <Modal
          header="Do you want Delete this wine in cart?"
          closeButton={true}
          background="green"
          action="delete"
          onClose={closeModal}
          btnOne="Delete"
          btnSecond="Cancel"
          onClick={deleteCard}
        />
      )}
    </>
  );
}

export default CartPage;
